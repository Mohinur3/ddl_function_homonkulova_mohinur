CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
SELECT
    fc.category_id,
    c.name AS category_name,
    COALESCE(SUM(p.amount), 0) AS total_sales_revenue
FROM
    film_category fc
JOIN film f ON fc.film_id = f.film_id
JOIN inventory i ON f.film_id = i.film_id
JOIN rental r ON i.inventory_id = r.inventory_id
JOIN payment p ON r.rental_id = p.rental_id
JOIN category c ON fc.category_id = c.category_id
WHERE
    EXTRACT(QUARTER FROM r.rental_date) = EXTRACT(QUARTER FROM CURRENT_DATE)
GROUP BY
    fc.category_id, c.name
HAVING
    COALESCE(SUM(p.amount), 0) > 0;

CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_quarter INT)
RETURNS TABLE (category_id INT, category_name VARCHAR(255), total_sales_revenue NUMERIC)
AS $$
BEGIN
    RETURN QUERY
    SELECT
        fc.category_id,
        c.name AS category_name,
        COALESCE(SUM(p.amount), 0) AS total_sales_revenue
    FROM
        film_category fc
    JOIN film f ON fc.film_id = f.film_id
    JOIN inventory i ON f.film_id = i.film_id
    JOIN rental r ON i.inventory_id = r.inventory_id
    JOIN payment p ON r.rental_id = p.rental_id
    JOIN category c ON fc.category_id = c.category_id
    WHERE
        EXTRACT(QUARTER FROM r.rental_date) = current_quarter
    GROUP BY
        fc.category_id, c.name
    HAVING
        COALESCE(SUM(p.amount), 0) > 0;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION new_movie(movie_title VARCHAR(255))
RETURNS VOID
AS $$
DECLARE
    new_film_id INT;
    language_id INT;
BEGIN
    SELECT COALESCE(MAX(film_id), 0) + 1 INTO new_film_id FROM film;

    SELECT language_id INTO language_id FROM language WHERE name = 'Klingon';
    IF language_id IS NULL THEN
        RAISE EXCEPTION 'Language Klingon does not exist.';
    END IF;

    INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (new_film_id, movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), language_id);

END;
$$ LANGUAGE PLPGSQL;